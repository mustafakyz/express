const express = require("express");
const app = express();
const favicon = require('serve-favicon');

var cors = require('cors');
var cookieParser = require('cookie-parser')

app.set('view engine', 'ejs');
app.set('views', require('path').join(__dirname, '/public/view'));
app.use(express.static('public/assets'))
var session = require('express-session');
app.use(cookieParser())
app.use(cors());

app.post('/webhook',function(
    req,
    res,
    next) {

    const { exec } = require('child_process');
    var yourscript = exec('sh commit.sh',
        (error, stdout, stderr) => {
            console.log(stdout);
            console.log(stderr);
            if (error !== null) {
                console.log(`exec error: ${error}`);
            }
        });
    res.send("success!").status(200);
});


const homeRoute = require("./app/routes/home_route");
const register = require("./app/routes/register");
const login = require("./app/routes/login");
const appointment = require("./app/routes/appointment");
const offer = require("./app/routes/offer");
const authentication = require("./app/middleware/authentication");

//node js ilk uğradığı route'a girer o yüzden / route en altta yazılır.routelarınızı bu kurala göre oluşturmanız gerekir
/* ilk olarak authentication middleware'ına uğramasını istedik istediğimiz için
    app.use() 'da authentication middlewareını çağırıyoruz başarısız durumda studentRoute uğramayacaktır;
    başka middlewarelarınız da olabilir bunun için hangi routelarda kullanmak istiyorsanız orada çağırmalısınız.
 */

app.use('/home',authentication,homeRoute);
app.use('/register',authentication,register);
app.use('/login',authentication,login);
app.use('/offer',authentication,offer);
app.use('/appointment',authentication,appointment);

app.get('/', (req, res) => res.redirect('/home'));

app.use((req, res, next) => {
    return res.status(404).end('Not Found');
});

module.exports = app;
