let Register = {}; //Bir object oluşturuyoruz ve model fonksiyonlarımızı döndürüyoruz;
//Static datalar verdim burada mysql,mongo, mssql gibi dblere bağlanıp oradan datayı alabilirsiniz

const mysql = require("../dataaccess/mysql");

Register.getCountries = async (query) =>{
   try {
      return  new Promise((resolve,reject) => {
         mysql.getConnection(function(err, connection) {
            //example of mysql
            if (err) throw err; // not connected!

            // Use the connection
            let c = connection.query('SELECT * FROM countries', function (error, results, fields) {

               // When done with the connection, release it.
               connection.release();

               // Handle error after the release.
               if (error) throw error;

               return resolve(results);
               // Don't use the connection here, it has been returned to the pool.
            });
         });
      })

   }catch (e){
      return "error...";
   }

}
Register.getStates = async (countryId) =>{
   try {
      return  new Promise((resolve,reject) => {
         mysql.getConnection(function(err, connection) {
            //example of mysql
            if (err) throw err; // not connected!

            // Use the connection
            let c = connection.query('SELECT * FROM states WHERE country_id =?', [countryId], function (error, results, fields) {

               // When done with the connection, release it.
               connection.release();

               // Handle error after the release.
               if (error) throw error;

               return resolve(results);
               // Don't use the connection here, it has been returned to the pool.
            });
         });
      })

   }catch (e){
      return "error...";
   }

}

module.exports = Register;
