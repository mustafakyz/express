let Register = {};
let model = require("../models/register"); //Modelimizi çağırdık

Register.getCountries = async  () =>{
     return Promise.resolve(await model.getCountries({})); //model objesi üzerinden model fonksiyonuna ulaştık
}
Register.getStates = async  (req) =>{
    return Promise.resolve(await model.getStates(req.params.countryId)); //model objesi üzerinden model fonksiyonuna ulaştık
}

module.exports = Register;
