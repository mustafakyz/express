const axios = require('axios');
const redis = require('redis');
const redisClient = redis.createClient();



redisClient.on('error', (err) => {
    console.log('Redis error: ', err);
});

class Helper
{
    async getRequest(url, method, headers, data)
    {
        return new Promise((resolve, reject) => {

            var config = {
                method: method,
                url: url,
                headers: headers,
                data : data
            };
            const https = require('https');
            const instance = axios.create({
                httpsAgent: new https.Agent({
                    rejectUnauthorized: false
                })
            });
            try
            {
                instance(config).then(function (response)
                {
                    return resolve(response.data);
                }).catch(function (error)
                {
                    return reject(error);
                });
            }
            catch (error)
            {
                return reject(error);
            }
        });
    }



   /* static async getCache(key)
    {
        return new Promise((resolve, reject) => {
            redisClient.get(key, (error, replay) => {
                if(error) return reject(error);
                return resolve(JSON.parse(replay));
            });
        })
    }
    */




}




module.exports = Helper;
