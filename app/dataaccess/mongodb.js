const config = require('../../config');
const mongoClient = require("mongodb").MongoClient;
const {ObjectID, ISO} = require("mongodb")
class  mongoDbClient {
    async connect(){
        try {
            var connection = await mongoClient.connect(config.mongo.url, { useNewUrlParser: true });
            this.db = connection.db(config.mongo.db);
            console.log("Başarı");

        }
        catch(error) {
            console.log(error);
        }
    }

    async getData(coll, query,mongoFname) {
        try {
            await  this.connect();
            if(!query){
                throw Error("mongoClient.findDocFieldsByFilter: query is not an object");
            }
            let result =  await this.db.collection(coll)[mongoFname](query);
            return result.toArray();
        }
        catch (error){
             console.log(error);
        }
    }

}

module.exports = mongoDbClient;
