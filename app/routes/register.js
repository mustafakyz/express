const express = require('express');

const router = express.Router();
const app = express();
const controller = require('../controller/register')


router.get('/', async function(req,res,next){
    let result = await controller.getCountries();
    res.render('register', {result: result});

})
router.get('/getStates/:countryId', async function(req,res,next){
    let result = await controller.getStates(req);
    res.send({result: result, status: "success"});
})

module.exports = router;
