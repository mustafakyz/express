const express = require('express');

const router = express.Router();
const app = express();
const mysql = require('../dataaccess/mysql');  // call mysql
router.get('/', async function(req,res,next){
    mysql.getConnection(function(err, connection) {
         //example of mysql
          if (err) throw err; // not connected!

          // Use the connection
          let c = connection.query('SELECT * FROM countries', function (error, results, fields) {

               // When done with the connection, release it.
               connection.release();

               // Handle error after the release.
               if (error) throw error;

                return results;
               // Don't use the connection here, it has been returned to the pool.
          });
     });

     res.render('home');
})
module.exports = router;
