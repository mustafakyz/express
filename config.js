let config = {
    mongo :
    {
            url: "mongodb://localhost:27017",
            db:"school",
            collections:{
                teacher: 'teachers',
                student : 'students'
            }
    }

}

module.exports = config;
